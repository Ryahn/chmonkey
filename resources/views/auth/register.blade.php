<!DOCTYPE html>
<html dir="ltr">
@include('layouts.head')

<body>
<div class="main-wrapper">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url({{ asset('assets/images/big/auth-bg.jpg') }} no-repeat center center;">
            <div class="auth-box">
                <div>
                    <div class="logo">
                        <span class="db"><img src="{{ asset('assets/images/logo-icon.png') }}" alt="logo" /></span>
                        <h5 class="font-medium m-b-20">{{ __('Register') }}</h5>
                    </div>
                    <!-- Form -->
                    <div class="row">
                        <div class="col-12">
                            <form class="form-horizontal m-t-20" method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="form-group row ">
                                    <div class="col-12 ">
                                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-lg" type="text" placeholder="{{ __('CH Username') }}" name="name" value="{{ old('name') }}" autofocus required>
                                    </div>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group row ">
                                        <div class="col-12 ">
                                            <input class="form-control{{ $errors->has('profile') ? ' is-invalid' : '' }} form-control-lg" type="url" placeholder="{{ __('Profile Link') }}" name="profile" value="{{ old('profile') }}" required>
                                        </div>
                                        @if ($errors->has('profile'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('profile') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                <div class="form-group row">
                                    <div class="col-12 ">
                                        <input class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }} form-control-lg" type="text" placeholder="{{ __('Username') }}" name="username" value="{{ old('username') }}" required>
                                    </div>
                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <div class="col-12 ">
                                        <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-lg" type="password" placeholder="{{ __('Password') }}" name="password" required>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <div class="col-12 ">
                                        <input class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }} form-control-lg" type="password" placeholder="{{ __('Confirm Password') }}" name="password_confirmation" required>
                                    </div>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                @if (env('APP_ENV') == 'production')
                                    <div class="form-group row">
                                        <div class="col-md-12 ">
                                                {!! NoCaptcha::display(['data-theme' => 'dark']) !!}
                                            </div>
                                        </div>
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                @endif
                                <div class="form-group text-center ">
                                    <div class="col-xs-12 p-b-20 ">
                                        <button class="btn btn-block btn-lg btn-info " type="submit ">{{ __('Register') }}</button>
                                    </div>
                                </div>
                                <div class="form-group m-b-0 m-t-10 ">
                                    <div class="col-sm-12 text-center ">
                                        Already have an account? <a href="authentication-login1.html " class="text-info m-l-5 "><b>Sign In</b></a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->
    <script src="{{ asset('assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    @if(env('APP_ENV') == 'production') {
        {!! NoCaptcha::renderJs() !!}
    }
    @endif
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->
    <script>
    $('[data-toggle="tooltip "]').tooltip();
    $(".preloader ").fadeOut();
    </script>
</body>

</html>
